﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UI : MonoBehaviour
{
    public static UI ins;
    public AppData data;

    public List<IScreen> screens;

    [ContextMenu("AddScreen")]
    void AddScreen()
    {
        screens = FindObjectsOfType<IScreen>().ToList();
    }

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        ShowScreen<AppScreen>();    
    }

    public static void ShowScreen<T>(params object[] args) where T : IScreen
    {
        foreach (var item in ins.screens)
        {
            if(item is T)
            {
                item.OnShow(args);
            }
            else
            {
                item.OnClose();
            }
        }
    }
}
