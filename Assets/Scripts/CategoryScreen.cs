﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CategoryScreen : IScreen
{
    public GameObject comicGo;
    public RectTransform content;
    public Button backBtn;

    private void Start()
    {
        backBtn.onClick.AddListener(Close);
    }

    private void Close()
    {
        UI.ShowScreen<AppScreen>();
    }

    public override void OnShow(params object[] args)
    {
        base.OnShow(args);
        Poolers.ins.ClearItem(comicGo);
        content.DetachChildren();
        var data = UI.ins.data.GetCategoryData((string)args[0]);
        foreach (var item in data.comics)
        {
            var go = Poolers.ins.GetObject(comicGo, content);
            go.Cast<ComicItem>().OnShow(item);
        }
    }
}