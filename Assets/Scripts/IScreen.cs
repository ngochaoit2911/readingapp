﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class IScreen : MonoBehaviour
{
    public CanvasGroup group;
    public float fadeInTime = 0.2f;
    public float fadeOutTime = 0.2f;

    private void OnValidate()
    {
        group = GetComponent<CanvasGroup>();
    }

    public virtual void OnShow(params object[] args)
    {
        this.Show();
        group.alpha = 0;
        group.DOFade(1, fadeInTime);
    }

    public void OnClose()
    {
        this.Hide();
    }
}
