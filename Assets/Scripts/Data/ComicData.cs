﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName ="Data/Comic", fileName ="ComicData")]
public class ComicData : ScriptableObject
{
    public string comicName;
    public string pathName;
    private void OnValidate()
    {
        comicName = this.name;
    }
}
