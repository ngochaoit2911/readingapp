﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Data/AppData", fileName ="AppData")]
public class AppData : ScriptableObject
{
    public List<CategoryDara> catalogies;

    public CategoryDara GetCategoryData(string s)
    {
        return catalogies.Find(t => t.categoryName == s);
    }

}
