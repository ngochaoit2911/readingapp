﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Data/Category", fileName ="CategoryData")]
public class CategoryDara : ScriptableObject
{
    public string categoryName;
    public List<ComicData> comics;

    private void OnValidate()
    {
        categoryName = this.name;
    }
}