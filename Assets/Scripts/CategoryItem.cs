﻿using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class CategoryItem : PoolItem
{
    public TextMeshProUGUI txt;
    public Button btn;
    private string categoryName;

    private void Start()
    {
        btn.onClick.AddListener(ShowCategory);
    }

    public void OnShow(string s)
    {
        this.Show();
        categoryName = s;
        txt.text = s;
    }

    private void ShowCategory()
    {
        UI.ShowScreen<CategoryScreen>(categoryName);
    }
}
