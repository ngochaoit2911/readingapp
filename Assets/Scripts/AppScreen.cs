﻿using System.Collections;
using UnityEngine;

public class AppScreen : IScreen
{
    public GameObject categoryGo;
    public RectTransform content;

    public override void OnShow(params object[] args)
    {
        base.OnShow(args);
        Poolers.ins.ClearItem(categoryGo);
        content.DetachChildren();
        foreach (var item in UI.ins.data.catalogies)
        {
            var go = Poolers.ins.GetObject(categoryGo, content);
            go.Cast<CategoryItem>().OnShow(item.categoryName);
        }
    }

}
