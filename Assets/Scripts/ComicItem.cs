﻿using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class ComicItem : PoolItem
{

    public TextMeshProUGUI txt;
    public Button btn;
    private ComicData data;

    private void Start()
    {
        btn.onClick.AddListener(ShowCategory);
    }

    public void OnShow(ComicData s)
    {
        this.Show();
        data = s;
        txt.text = s.comicName;
    }

    private void ShowCategory()
    {

    }
}
